package com.xvjialing.jkforum.jkforum.controller;

import com.xvjialing.jkforum.jkforum.bean.Novel;
import com.xvjialing.jkforum.jkforum.service.NovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/api/jkforum/v1")
public class NovelController {

    @Autowired
    NovelService novelService;

    @RequestMapping("/novel/{id}")
    public String findOneNovel(@PathVariable("id") int id,ModelMap map){
        Novel novel=novelService.getNovel(id);
        Map map1=new HashMap();
        map1.put("title",novel.getTitle());
        map1.put("content",novel.getContent());
        map1.put("id",novel.getId());
        map.addAllAttributes(map1);
        return "novel";
    }

//    @RequestMapping("/novel/list/{page}")
//    public String
}