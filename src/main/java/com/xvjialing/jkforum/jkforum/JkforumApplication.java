package com.xvjialing.jkforum.jkforum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JkforumApplication {

	public static void main(String[] args) {
		SpringApplication.run(JkforumApplication.class, args);
	}
}
