package com.xvjialing.jkforum.jkforum.service;

import com.xvjialing.jkforum.jkforum.bean.Novel;
import com.xvjialing.jkforum.jkforum.repositoty.NovelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
public class NovelService {

    @Autowired
    private NovelRepository novelRepository;

    public Novel getNovel(Integer id){
        return novelRepository.findOne(id);
    }

    public Page<Novel> getNovels(@PageableDefault(value = 1) Pageable pageable){
        return novelRepository.findAll(pageable);
    }
}