package com.xvjialing.jkforum.jkforum.repositoty;

import com.xvjialing.jkforum.jkforum.bean.Novel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NovelRepository extends JpaRepository<Novel,Integer> {


}
