FROM maven:3.5.2-jdk-8-alpine AS development
RUN mkdir -p /app
WORKDIR /app
COPY . .
RUN mvn clean package -Dmaven.test.skip=true

FROM openjdk:jre-alpine AS production

VOLUME /tmp

COPY --from=development /app/target/jkforum-0.0.1-SNAPSHOT.jar  app.jar

EXPOSE 8086

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar","--spring.profiles.active=docker"]

#FROM java:8-jre-alpine
##RUN mkdir -p /app
##WORKDIR /app
#ADD target/lytech_sensors-0.0.1-SNAPSHOT.jar /app.jar
#EXPOSE 8086
#ENTRYPOINT ["java","-jar","/app.jar"]

#FROM openjdk:jre-alpine
#VOLUME /tmp
#ADD target/jkforum-0.0.1-SNAPSHOT.jar /app.jar
#EXPOSE 8086
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar","--spring.profiles.active=docker"]